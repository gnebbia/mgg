# mgg.  MAL graph generator

[![Build Status](https://travis-ci.com/gnebbia/mgg.svg?branch=master)](https://travis-ci.com/gnebbia/mgg)

Author: Giuseppe Nebbione <nebbione@kth.se>

Copyright: © 2021, Giuseppe Nebbione.

Date: 2021-11-08

Version: 0.1.0


## PURPOSE

The mgg platform generates a full attack graph starting from a securiCAD
model and a MAL Language specification compiled with malc.

## INSTALLATION

We can install mgg simply by doing:
```sh
git clone https://github.com/gnebbia/mgg
cd mgg
python -m venv env
source env/bin/activate
pip install -r requirements.txt
```

NOTE: You need a python version >= 3.9, the latest the better.


## USAGE
To run a dummy session and test that everything is working correctly
we need an open Neo4J instance running with a database
with the following credentials:

- username: neo4j
- password: mgg12345!
- dbname:   neo4j

Note that the Neo4J project name and the DBMS name used within
the project are irrelevant, the most important thing is that
the password matches as the username and specific database
name are set to default values.

The mentioned credentials are just for testing purposes and may be changed.

Once neo4j is setup we can perform a test run with:

```sh
python -m mgg interactive
```

```sh
make run
```

If we want to generate an attack graph starting from a .sCAD archive and
a .mar language specification we can do:

```sh
python -m mgg scad /path/to/model.sCAD path/to/mal-lang.coreLang-0.3.0.mar
```

For example with a model/language contained in the test directory:
```sh
python -m mgg scad tests/assets/coreLang/models/petest4.sCAD tests/assets/coreLang/org.mal-lang.coreLang-0.3.0.mar
```

Once the program is completed by default the output attack graph is stored in the tmp/ directory.
The attack graph is stored in the json format as "tmp/atkgraph.json".


## NOTES

## UPGRADE

## ADDITIONAL NOTES

We can find compilable testlang(s) at:
https://gitlab.com/gnebbia/testlangs

## COPYRIGHT

Copyright 2021, Giuseppe Nebbione.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

