# mgg.  Convert a SecuriCAD instance project to a reusable model


Author: Giuseppe Nebbione <nebbione@kth.se>

Copyright: © 2021, Giuseppe Nebbione.

Date: 2021-11-08

Version: 0.1.0


## PURPOSE


## INSTALLATION

We can install mgg simply by doing:
```sh
git clone https://github.com/gnebbia/mgg
cd mgg
pip install -r requirements.txt
python setup.py install
```



## USAGE

## NOTES

## UPGRADE

## DONATIONS

I am an independent developer working on mgg in my free time,
if you like mgg and would like to say thank you, buy me a beer!

[![paypal](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=nebbionegiuseppe%40gmail.com&currency_code=EUR&source=url)

## COPYRIGHT

Copyright 2021, Giuseppe Nebbione.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
