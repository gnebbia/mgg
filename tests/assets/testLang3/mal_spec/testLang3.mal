/*
 * Copyright 2020-2021 Foreseeti AB <https://foreseeti.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#id: "org.mal-lang.testlang3"
#version: "1.0.0"

category General {
  asset Cable {
    let allTraffic = inTraffic \/ outTraffic

    | z1
      -> z2,z3
    | z2
      -> allTraffic().read
    | z3
      -> inTraffic.onea2
  }

  asset Traffic {
    | read
        -> onea1
    | onea1
    | onea2
  }


  asset Port {
    | access
        -> writeData
    | writeData
        -> comp.write
  }

  asset Computer {
     | write
  }

}

associations {
  Cable [cableInTraffic]  * <-- InTraffic  --> * [inTraffic]  Traffic
  Cable [cableOutTraffic] * <-- OutTraffic --> * [outTraffic] Traffic
  Port  [port]            * <-- Rel3 --> * [comp]       Computer
}
