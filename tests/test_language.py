#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# mgg test suite
# Copyright © 2021, Giuseppe Nebbione.
# See /LICENSE for licensing information.


import pytest
import mgg
import mgg.model as m
import mgg.language as l
import mgg.securicad as s



@pytest.fixture
def model():
    return s.load_model_from_scad_archive("tests/assets/coreLang/models/petest4.sCAD")

@pytest.fixture
def lang():
    return s.load_language_specification("tests/assets/coreLang/org.mal-lang.coreLang-0.3.0.mar")


@pytest.fixture
def model_lang6():
    return s.load_model_from_scad_archive("tests/assets/testLang6/models/1OS_1Linux_Spyware_on_both.sCAD")

@pytest.fixture
def lang6():
    return s.load_language_specification("tests/assets/testLang6/org.mal-lang.testlang6-1.0.0.mar")


def test_load_language_specification(lang):
    assert list(lang.keys()) == ['formatVersion', 'defines', 
                           'categories', 'assets', 'associations']


def test_get_classes(lang):
    assert len(l.get_classes(lang)) == 18

def test_get_super_class(lang):
    assert ['SoftwareVulnerability','Vulnerability'] ==  l.get_super_classes(lang, "UnknownSoftwareVulnerability")



def test_get_attacks_for_class(lang):
    object_class = "SoftwareVulnerability"
    sys_atks = l.get_attacks_for_class(lang,object_class)
    assert len(sys_atks.keys()) == 27

    object_class = "System"
    sys_atks = l.get_attacks_for_class(lang,object_class)
    print(sys_atks.keys())
    # Add assert here

    object_class = "Vulnerability"
    vuln_atks = l.get_attacks_for_class(lang,object_class)
    print(vuln_atks.keys())
    # Add assert here

    object_class = "Application"
    app_atks = l.get_attacks_for_class(lang,object_class)
    print(app_atks.keys())
    # Add assert here

def test_get_defense_for_class(lang):
    object_class = "Application"
    app_defs = l.get_defenses_for_class(lang,object_class)
    assert len(app_defs.keys()) == 3



def test_get_associations_for_class(lang):
    # A network should have 7 associations
    object_class = "Network"
    network_assocs = l.get_associations_for_class(lang,object_class)
    assert len(network_assocs) == 7

    # A routing Firewall is an Application
    # We should get all the assocs also available for applications here
    object_class = "RoutingFirewall"
    rfw_assocs = l.get_associations_for_class(lang,object_class)
    # The whole set of associations for RoutingFirewall should be 22 elements
    assert len(rfw_assocs) == 22


    object_class = "Application"
    app_assocs = l.get_associations_for_class(lang,object_class)
    # The whole set of associations for Application should be 21 elements
    assert len(app_assocs) == 21


def test_build_attack_step_name(lang):
    object_class = "System"
    sys_atks = l.get_attacks_for_class(lang,object_class)
    atksteps = []
    atk_name = "fullAccess"
    for atkstep in sys_atks[atk_name]["reaches"]["stepExpressions"]:
            atksteps.append(l.build_attack_step_name(atkstep))

    assert atksteps == ['sysExecutedApps.fullAccess', 
                        'highPrivSysIds.assume',
                        'lowPrivSysIds.assume', 
                        'highPrivSysGroups.compromiseGroup',
                        'lowPrivSysGroups.compromiseGroup',
                        'sysData.attemptAccess']




def test_is_internal_attack_step():
    int_atkname = "compromise"
    ext_atkname1 = "app.compromise"
    ext_atkname2 = "app.fw.compromise"
    complex_atkname = "app()"
    assert l.is_internal_attack_step(int_atkname)
    assert not l.is_internal_attack_step(ext_atkname1)
    assert not l.is_internal_attack_step(ext_atkname2)
    assert not l.is_internal_attack_step(complex_atkname)

    
def test_get_child_steps(lang):
    atk_name = "successfulAccess"
    obj_class = "Network"
    atksteps = l.get_child_steps(lang,obj_class,atk_name)
    
    """Excerpt from coreLang asset Network
       | successfulAccess @hidden
         -> allowedApplicationConnections().attemptConnectToApplications,
            allowedApplicationConnections().attemptTransmit,
            applications.networkConnect,
            applications.networkRequestConnect,
            clientApplications.networkRespondConnect,
            accessNetworkData,
            networkForwarding,
            denialOfService,
            attemptReverseReach
    """
    assert atksteps == ["allowedApplicationConnections().attemptConnectToApplications",
                        "allowedApplicationConnections().attemptTransmit",
                        "applications.networkConnect",
                        "applications.networkRequestConnect",
                        "clientApplications.networkRespondConnect",
                        "accessNetworkData",
                        "networkForwarding",
                        "denialOfService",
                        "attemptReverseReach"]



def test_get_child_steps_with_square_brackets(lang):
    atk_name = "overrideCredentials"
    obj_class = "Data"
    atksteps = l.get_child_steps(lang,obj_class,atk_name)

    assert atksteps == ['information[Credentials].attemptAccess']

def test_get_child_steps_with_asterisk(lang):
    atk_name = "propagateOneCredentialCompromised"
    obj_class = "Credentials"
    atksteps = l.get_child_steps(lang,obj_class,atk_name)

    assert atksteps == ['credentials*.identities.users.oneCredentialCompromised']






def test_get_internal_parents(lang):
    target_atk = "attemptGainFullAccess"
    target_cls = "System"
    internal_steps = l.get_internal_parents(lang, target_cls, target_atk)
    assert ("-",'connect') in internal_steps
    assert ("-",'allPrivilegeAuthenticate') in internal_steps
    assert len(internal_steps) == 2



def test_is_step_resolvable(lang):
    atkname = "sysExecutedApps.fullAccess"
    origin_cls = "System"
    target_cls = "Application"
    target_atk = "fullAccess"
    assert "." in atkname and atkname.split(".")[-1] == target_atk
    assert l.is_step_resolvable(lang, origin_cls, target_cls, atkname)

    atkname = "sysExecutedApps.fullAccess"
    origin_cls = "SoftwareVulnerability"
    target_cls = "Application"
    target_atk = "fullAccess"
    assert "." in atkname and atkname.split(".")[-1] == target_atk
    assert not l.is_step_resolvable(lang, origin_cls, target_cls, atkname)

    atkname = "appSoftProduct.softProductVulnerabilities.networkAccessAchieved"
    origin_cls = "Application"
    target_cls = "SoftwareVulnerability"
    target_atk = "networkAccessAchieved"
    assert "." in atkname and atkname.split(".")[-1] == target_atk
    assert l.is_step_resolvable(lang, origin_cls, target_cls, atkname)



def test_get_parent_steps(lang):
    atkname = "appSoftProduct.softProductVulnerabilities.networkAccessAchieved"
    target_cls = "SoftwareVulnerability"
    target_atk = "networkAccessAchieved"
    parents = l.get_parent_steps(lang, target_cls, target_atk)

def test_inheritance_overrides(model_lang6, lang6):
    object_class = "Linux"
    sys_atks = l.get_attacks_for_class(lang6,object_class)
    atksteps = []
    atk_name = "spyware"
    atksteps = l.get_child_steps(lang6,object_class,atk_name)

    assert atksteps == ['readRootCreds','logKeystrokes']


