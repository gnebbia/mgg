# These functions were discarded because reachability analysis
# was not considered important
def is_node_reachable(lang: dict, graph: List[AtkGraphNode], node: AtkGraphNode) -> bool:
    if node.type == "and":
        return is_and_node_reachable(lang,graph,node)
    elif node.type == "or":
        return is_or_node_reachable(graph,node)


def is_or_node_reachable(graph: List[AtkGraphNode], node: AtkGraphNode) -> bool:
    parents = get_node_parents(graph, node.id)
    return any([get_node_by_id(graph, parent).is_reachable for parent in parents])


def is_and_node_reachable(lang: dict, graph: List[AtkGraphNode], node: AtkGraphNode) -> bool:
    if not node.type == "and":
        return True
    def is_internal_parent_satisfied(node, atkgraph_node_parents, parent_name):
        for node_parent in atkgraph_node_parents:
            cls, objid, atkname = node_parent.split(":")
            if objid == node.objid and atkname == parent_name:
                return True
        return False

    atkgraph_node_parents = get_node_parents(graph, node.id)
    for parent in atkgraph_node_parents:
        parent_node = get_node_by_id(graph, parent)
        if not parent_node.is_traversable:
            return False

    req_parents = l.get_parent_steps(lang, node.objclass, node.atkname)

    req_internal_parents = [parent for parent in req_parents if parent[0] == "-"]
    req_external_parents = [parent for parent in req_parents if parent[0] != "-"]

    for parent in req_internal_parents:
        parent_name = parent[1]
        if not is_internal_parent_satisfied(node, atkgraph_node_parents, parent_name):
            return False

    for parent in req_external_parents:
        cls = parent[0]
        atkname = parent[1]
        objects_to_check = get_nodes_by_class(graph, cls)
        parents_to_satisfy = [obj for obj in objects_to_check if obj.endswith(":"+atkname)]
        for parent in parents_to_satisfy:
            if parent not in atkgraph_node_parents:
                return False
    return True


def attach_attacker_and_compute(lang: dict, graph: List[AtkGraphNode], node_ids: List[str]) -> List[AtkGraphNode]:
    def set_reachability(lang, graph):
        newgraph = []
        for node in graph:
            newnode = node
            if newnode.id not in node_ids:
                newnode.is_reachable = is_node_reachable(lang, graph, node)
            newgraph.append(newnode)
        return newgraph

    for attached_node in node_ids:
        graph = attach_attacker_to_node(graph, attached_node)
    graph = set_reachability(lang, graph)
    return graph



def prune_untraversable(graph: List[AtkGraphNode]) -> List[AtkGraphNode]:
    newgraph = []
    nodes_to_remove = []
    for node in graph:
        if node.is_traversable:
            newgraph.append(node)
        else:
            nodes_to_remove.append(node.id)

    for node in graph:
        for link in nodes_to_remove:
            if link in node.links: node.links.remove(link)
    return newgraph


def prune_unreachable(graph: List[AtkGraphNode]) -> List[AtkGraphNode]:
    newgraph = []
    nodes_to_remove = []
    for node in graph:
        if node.is_reachable:
            newgraph.append(node)
        else:
            nodes_to_remove.append(node.id)
    for node in graph:
        for link in nodes_to_remove:
            if link in node.links: node.links.remove(link)
    return newgraph



def get_nodes_by_class(graph, cls):
    nodes = select(where(cls=cls), graph)
    objs = set()
    for node in nodes:
        objs.add(node.id)
    return list(objs)



def attach_attacker_to_node(graph: List[AtkGraphNode], node_id: str) -> List[AtkGraphNode]:
        node = get_node_by_id(graph, node_id)
        node.is_reachable = True
        visited_nodes = []

        def traverse(node):
            if node.id in visited_nodes:
                return
            node.is_traversable = True
            visited_nodes.append(node.id)
            for link in node.links:
                linknode = get_node_by_id(graph, link)
                traverse(linknode)

        traverse(node)
        return graph

