# -*- encoding: utf-8 -*-
# mgg v0.1.0
# Generate MAL-based attack graphs
# Copyright 2021, Giuseppe Nebbione.
# See /LICENSE for licensing information.

"""
mgg main Module

:Copyright: 2021, Giuseppe Nebbione.
:License: GPLv3 (see /LICENSE).
"""

__all__ = ()

import sys
from mgg.cl_parser import parse_args

import securicad
import mgg.model as m
import mgg.language as l
import mgg.securicad as s


def main():
    """Main routine of mgg."""

    print("mgg")
    args = parse_args(sys.argv[1:])
    cmd_params = vars(args)
    print(cmd_params)

config_file = cmd_params["config_file"]
lang_file = cmd_params["language_file"]
file_spec = "tests/assets/corelang_v0.3.0.json"
config_file = "/home/gnc/coa_gn.ini"


client = s.enterprise_connect_from_config(config_file)
model = s.get_model_from_enterprise(client, "test", "PE test4")

s.download_scad_model( client, "test", "PE test4")
m.save_model_to_json(model,"PE_test4.json")

objects = m.get_objects(model)
print(objects)

lang = l.read_language_spec(file_spec)
super_classes = l.get_super_classes(lang, "UnknownSoftwareVulnerability")



objects = m.get_objects(model)
print(objects)

links = m.get_links(model)
print(links)

object_id = "6417249322885591"
links_for_x = m.get_links_for_object(model,object_id)
links_for_x
in_links_for_x = m.get_in_links_for_object(model,object_id)
out_links_for_x = m.get_out_links_for_object(model,object_id)

object_class = m.get_class(model,object_id)

file_spec = "tests/assets/corelang_v0.3.0.json"
lang = l.read_language_spec(file_spec)


object_class = "SoftwareVulnerability"
sw_vuln_atks = l.get_attacks_for_class(lang,object_class)
print(sw_vuln_atks.keys())


object_class = "System"
sys_atks = l.get_attacks_for_class(lang,object_class)
print(sys_atks.keys())

object_class = "Vulnerability"
vuln_atks = l.get_attacks_for_class(lang,object_class)
print(vuln_atks.keys())

object_class = "Application"
app_atks = l.get_attacks_for_class(lang,object_class)
print(app_atks.keys())




# Get all attack steps for Asset = System; atkstep = fullAccess (no variables)
object_class = "System"
sys_atks = l.get_attacks_for_class(lang,object_class)
print(sys_atks.keys())

atksteps = []
atk_name = "fullAccess"
for atkstep in sys_atks[atk_name]["reaches"]["stepExpressions"]:
    atksteps.append(l.build_attack_step_name(atkstep))

atksteps
# Get all attack steps for Asset = Application; atkstep = fullAccess (variables)
object_class = "Application"
app_defs = l.get_defenses_for_class(lang,object_class)
print(app_defs.keys())

atksteps = []
atk_name = "fullAccess"
for atkstep in app_atks[atk_name]["reaches"]["stepExpressions"]:
    print(l.build_attack_step_name(atkstep))




object_class = "Network"
network_assocs = l.get_associations_for_class(lang,object_class)
assert len(network_assocs) == 7

# A routing Firewall is an Application
# We should get all the assocs also available for applications here
object_class = "RoutingFirewall"
rfw_assocs = l.get_associations_for_class(lang,object_class)
# The whole set of associations for RoutingFirewall should be 22 elements
assert len(rfw_assocs) == 22 


object_class = "Application"
app_assocs = l.get_associations_for_class(lang,object_class)
# The whole set of associations for Application should be 21 elements
assert len(app_assocs) == 21





cplx_atk4 = "appSoftProduct.softProductVulnerabilities.localAccessAchieved"
obj_id = "7219598629313512" # Application which is then connected to 2 vulns
l.build_links_from_external_attack_step(model,lang, obj_id, cplx_atk4)

# These can be used for other tests
cplx_atk1 = "applications.networkConnect"
cplx_atk2 = "applications.networkRequestConnect"
cplx_atk3 = "clientApplications.networkRespondConnect"



# TODO FUNCTION
# def get_attacks_for_object(model,lang,object_id):
object_id = "8176711980537409" # Network
object_id = "7219598629313512" # Application which is then connected to 2 vulns


### BEGIN MAIN 

nodes = []

for obj_id in objects:
    print("Processing Object ID: " + obj_id)
    obj_class = m.get_class(model, obj_id)
    print("Class: " + obj_class)
    atks = l.get_attacks_for_class(lang,obj_class)
    print("Attacks:")
    print(atks.keys())
    for atk_name in atks:
        print("This attack is: " + atk_name)
        print("Belonging to: " + obj_class)
        atksteps = l.get_attack_steps_for_attack(lang,obj_class,atk_name)
        print("Attack Steps for attack: " + atk_name + " are:")
        print(atksteps)
        links = []
        for atkstep in atksteps:
            if "()" in atkstep:
                print("Skipped Attack Step: " + atkstep)
            elif l.is_internal_attack_step(atkstep):
                # connected_atksteps.append(obj_id + ":" + atkstep)
                print("Internal Attack Step: " + atkstep)
                links.append(l.build_link_from_internal_attack_step(obj_id, atkstep))
            else:
                print("External Attack Step: " + atkstep)
                links += l.build_links_from_external_attack_step(model, lang, obj_id, atkstep)
        print(links)
        l.insert_node(nodes, obj_id + ":" + atk_name, atks[atk_name]["type"],atks[atk_name]["ttc"], links)


import json
with open('output.json', 'w') as f:
    json.dump(nodes, f, indent=4)



import networkx as nx
nodes
g = nx.DiGraph()
for node in nodes:
    g.add_node(node["id"], type=node["type"],ttc=node["ttc"])
    for link in node['links']:
        print("Connecting " + node["id"] + " to " + link)
        g.add_edge(node["id"],link)


# Access node properties
g.nodes['929864580059290:exploitWithEffort']

g.edges()
import matplotlib.pyplot as plt
from pyvis.network import Network

net = Network(notebook=True)

net.from_nx(g)
net.show("example.html")



nx.draw(g)
plt.show()
plt.savefig("path.png")
### END MAIN 






for l in links:
    if l["type2"] == complex_atk[0]:
        linkz.append(l["id2"] + ":" + complex_atk[1])
        

assocs = get_associations_by_field(lang,obj_class,complex_atk[0])
print(assocs)
cassocs = get_associations_for_class(lang,obj_class)

links = get_links_for_object(model,obj_id)
links = get_links_for_object_by_class

assocs2 = get_associations_for_object(model,obj_id)

for assoc in assocs2:
    if complex_atk[0] == assoc["type"] and assoc["id"]




nodes = []
# Loop for all the attacks in the category:
# create a node for each attack
for atk in net_atks:
    print(net_atks[atk]["name"])
    atk_name = net_atks[atk]["name"]

    connected_atksteps = []
    atksteps = get_attack_steps_for_attack(lang,object_class,atk_name)
    for atkstep in atksteps:
        if is_internal_attack_step(atkstep):
            connected_atksteps.append(object_id + ":" + atkstep)
        else:
            resolve_atkstep(object_id,atkstep)


l.insert_node(nodes,
        object_id + ":" + atk_name,
        net_atks[atk]["type"],
        net_atks[atk]["ttc"],
        connected_atksteps)





if __name__ == "__main__":
    # Create an authenticated enterprise client
    client = scad_connect_from_config(sys.argv[1])
    project = client.projects.get_project_by_name("test")
    models = enterprise.models.Models(client)

    model_info = models.get_model_by_name(project, "easy2")
    # TODO get the model from simulation id
    print("Project Name: ", project)
    print("Model Name:   ", model_info.name)
    model = model_info.get_model()
    print("MODEL")
    print(json.dumps(model.model,indent=4))
    print("END MODEL")

    entities = get_entities(model)
    print(entities)
    # generate_attack_graph(model)



