# -*- encoding: utf-8 -*-
# mgg v0.1.0
# Generate MAL-based attack graphs
# Copyright 2021, Giuseppe Nebbione.
# See /LICENSE for licensing information.

"""
mgg rldefender plugin Module

:Copyright: 2021, Giuseppe Nebbione.
:License: GPLv3 (see /LICENSE).
"""

__all__ = ()

import yaml


def ingest(graph: list, output_file: str) -> None:

    yaml_tree = {}
    for node in graph:
        yaml_tree[node.id] = {}
        yaml_tree[node.id]['step_type'] = node.type
        yaml_tree[node.id]['ttc'] = node.ttc
        yaml_tree[node.id]['children'] = node.links


    with open(output_file, 'w') as fh:
        yaml.dump(yaml_tree, fh)
