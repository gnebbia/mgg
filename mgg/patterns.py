import json
import importlib
import securicad
import mgg.securicad as s
import mgg.language as l
import mgg.model as m
import mgg.atkgraph as a
from mgg.node import AtkGraphNode
from py2neo import Graph, Node, Relationship, Subgraph




def load_patterns(filename: str) -> dict:
    """
    Load patterns file from a json file

    Arguments:
    filename        - the path of the input file to parse
    """
    raw_patterns = {}
    with open(filename, 'r', encoding='utf-8') as file:
        raw_patterns = json.load(file)

    patterns = raw_patterns.copy()
    for pattern,attribs in raw_patterns.items():
        patterns[pattern] = {}
        patterns[pattern]['badPattern'] = ' '.join(attribs['badPattern'])
        patterns[pattern]['mitigation'] = ' '.join(attribs['mitigation'])
        patterns[pattern]['badAtkGraphPattern'] = ' '.join(attribs['badAtkGraphPattern'])
        patterns[pattern]['description'] = attribs['description']

    return patterns




def analyze_bad_patterns(model: dict,
        patterns,
        uri="bolt://localhost:7687", 
        username="neo4j",
        password="mgg",
        dbname="neo4j"
) -> None:
    """
    Analyze bad patterns on a model

    Arguments:
    model                - the model on which to apply a query
    patterns             - a patterns dictionary as loaded from the load_patterns function
    uri                  - the URI to a running neo4j instance
    username             - the username to login on Neo4J
    password             - the password to login on Neo4J
    dbname               - the selected database
    """

    g = Graph(uri=uri, user=username, password=password, name=dbname)  
    for name,attribs in patterns.items():
        print("Analyzing pattern on model: " + str(name))
        results = apply_query(model, attribs['badPattern'])
        print("Analyzing patternon on atkgraph: " + str(name))
        #results2 = apply_query(model, attribs['badAtkGraphPattern'])
        print("This bad pattern is present " + str(len(results)) + " times")
        for path in results:
            for subpath in path:
                print_match(subpath, path)



def apply_query(model: dict, 
        query, uri="bolt://localhost:7687",
        username="neo4j", 
        password="mgg",
        dbname="neo4j"
) -> None:
    """
    Apply a query on the Neo4J platform

    Arguments:
    model                - the model on which to apply a query
    query                - the query to issue to Neo4J
    uri                  - the URI to a running neo4j instance
    username             - the username to login on Neo4J
    password             - the password to login on Neo4J
    dbname               - the selected database

    Return:
    Return value is what would a  Neo4J query would return.
    For example:
    - in case of SELECT/MATCH queries the matching records are returned
    - in case of CREATION/UPDATE queries no record is returned
    """

    g = Graph(uri=uri, user=username, password=password, name=dbname)  
    return g.run(query).data()



def apply_all_mitigations(model: dict,
        mitigations,
        uri="bolt://localhost:7687",
        username="neo4j",
        password="mgg",
        dbname="neo4j"
) -> None:
    """
    Apply all mitigation queries (TODO)

    Arguments:
    model                - the model on which to apply a query
    query                - the query to issue to Neo4J
    uri                  - the URI to a running neo4j instance
    username             - the username to login on Neo4J
    password             - the password to login on Neo4J
    dbname               - the selected database
    """
    pass


def print_match(name_of_match: str, result_path: str):
    """
    Print detected match, this is mostly a debug function

    Arguments:
    name_of_match        - the name of the match
    result_path          - the result path to print
    """
    path = result_path[name_of_match]
    print("Printing: " + str(name_of_match))
    print(path)
    nodes = path.nodes
    relationshis = path.relationships   
    path_text = ""
    for n,r in zip(nodes, relationshis):
        path_text += "{} - {} - ".format(n['name'], type(r).__name__)

    path_text += nodes[-1]['name'] + '\n'
    print(path_text)
