# -*- encoding: utf-8 -*-
# mgg v0.1.0
# Generate MAL-based attack graphs
# Copyright 2021, Giuseppe Nebbione.
# See /LICENSE for licensing information.

"""
mgg atkgraph Module

:Copyright: 2021, Giuseppe Nebbione.
:License: GPLv3 (see /LICENSE).
"""

__all__ = ()

import json
import mgg.model as m
import mgg.language as l
from typing import List
from mgg.node import AtkGraphNode





def save_atkgraph(graph: List[AtkGraphNode], filename: str):
    """
    Save an mgg attack graph to a json file

    Arguments:
    graph           - an mgg attack graph generated with
                      generate_graph() function or loaded
                      from another file
    filename        - the name of the output file to be saved
    """
    serialized_graph = []
    for node in graph:
        serialized_graph.append(node.to_dict())
    with open(filename, 'w', encoding='utf-8') as file:
        json.dump(serialized_graph, file, indent=4)

def load_atkgraph(filename: str) -> AtkGraphNode:
    """
    Load an mgg attack graph model from a json file

    Arguments:
    filename        - the name of the input file to parse
    """
    with open(filename, 'r', encoding='utf-8') as file:
        serialized_graph = json.load(file)
    atkgraph = []
    for node in serialized_graph:
        atkgraph.append(AtkGraphNode(**node))
    return atkgraph

def add_node_to_graph(graph: List[dict], node: AtkGraphNode) -> List[AtkGraphNode]:
    """
    Insert a node in the current attack graph

    Arguments:
    graph       - the current attack graph data structure, 
                  that is a list of dictionaries
    node        - an attack graph node
    """
    graph.append(node)
    return graph

def build_link_from_internal_attack_step(obj_id: str, attack_name: str, cls: str) -> str:
    """
    Returns a string representing a node ID in an attack graph.
    The format is: `assetId:atkname`

    Arguments:
    object_id       - the ID of the asset object
    attack_name     - a string representing an attack_name


    Return:
    A tuple in the format (ObjectID:AttackName, class)
    """
    return (cls + ":" + obj_id + ":" + attack_name)


def build_links_from_external_attack_step(model, obj_id: str, attack_name: str) -> list:
    """
    Returns a list of strings representing node IDs in an attack graph.
    The format is: `assetId:atkname`

    Arguments:
    model           - a SecuriCAD Model object
                      this can be retrieved for example by:
                      model = model_info.get_model()
    obj_id          - the ID of the asset object
    attack_name     - a string representing an external attack name


    Return:
    A list of tuples in the format (ObjectID:AttackName, class).
    """
    complex_atk = attack_name.split(".")
    connections = []

    def _resolve(complex_atk, obj_id):
        obj_links = m.get_links_for_object(model, obj_id)
        if len(complex_atk) == 2:
            assoc = complex_atk.pop(0)
            for link in obj_links:
                if obj_id == link["id1"] and link["type2"] == assoc:
                    obj_class = m.get_class(model, link["id2"])
                    connections.append(obj_class + ":" + link["id2"] + ":" + complex_atk[0])
                elif obj_id == link["id2"] and link["type1"] == assoc:
                    obj_class = m.get_class(model, link["id1"])
                    connections.append(obj_class + ":" + link["id1"] + ":" + complex_atk[0])
        elif len(complex_atk) > 2:
            assoc = complex_atk.pop(0)
            for link in obj_links:
                # here we could also use out-links
                if obj_id == link["id1"] and link["type2"] == assoc:
                    next_obj_id = link["id2"]
                    _resolve(complex_atk, next_obj_id)
                elif obj_id == link["id2"] and link["type1"] == assoc:
                    next_obj_id = link["id1"]
                    _resolve(complex_atk, next_obj_id)

    _resolve(complex_atk, obj_id)
    return connections


def build_links_from_variable_attack_step(model, obj_id: str, asset_name: str) -> list:
    """
    Returns a list of strings representing partial node IDs in an attack graph.
    This function works on attack steps which contain the MAL variables, 
    e.g., AllRelations().read
    The format of the return value is: `objectClass:objectID`.
    The format is: `objectClass:objectID`

    Arguments:
    model           - a SecuriCAD Model object
                      this can be retrieved for example by:
                      model = model_info.get_model()
    obj_id          - the ID of the asset object
    asset_name      - a string representing the asset_name


    Return:
    A list of tuples in the format (objectClass:objectID).
    """
    complex_atk = asset_name.split(".")
    assets = []

    def _resolve(complex_atk, obj_id):
        obj_links = m.get_links_for_object(model, obj_id)
        if len(complex_atk) == 1:
            assoc = complex_atk.pop(0)
            for link in obj_links:
                if obj_id == link["id1"] and link["type2"] == assoc:
                    obj_class = m.get_class(model, link["id2"])
                    assets.append(obj_class + ":" + link["id2"])
                elif obj_id == link["id2"] and link["type1"] == assoc:
                    obj_class = m.get_class(model, link["id1"])
                    assets.append(obj_class + ":" + link["id1"])
        elif len(complex_atk) > 1:
            assoc = complex_atk.pop(0)
            for link in obj_links:
                if obj_id == link["id1"] and link["type2"] == assoc:
                    next_obj_id = link["id2"]
                    _resolve(complex_atk, next_obj_id)
                elif obj_id == link["id2"] and link["type1"] == assoc:
                    next_obj_id = link["id1"]
                    _resolve(complex_atk, next_obj_id)

    _resolve(complex_atk, obj_id)
    return assets



def build_links_from_square_attack_step(model, obj_id: str, asset_name: str) -> list:
    """
    Returns a list of strings representing partial node IDs in an attack graph.
    This function works on attack steps which contain the MAL inheritance
    operator, e.g., Software[WordProcessors].read
    The format of the return value is: `objectClass:objectID`.

    Arguments:
    model           - a SecuriCAD Model object
                      this can be retrieved for example by:
                      model = model_info.get_model()
    obj_id          - the ID of the asset object
    asset_name      - a string representing the asset_name


    Return:
    A list of tuples in the format (objectClass:objectID).
    """
    complex_atk = asset_name.split(".")
    assets = []

    def _resolve(complex_atk, obj_id):
        obj_links = m.get_links_for_object(model, obj_id)
        if len(complex_atk) == 1:
            assoc = complex_atk.pop(0)
            for link in obj_links:
                if obj_id == link["id1"] and link["type2"] == assoc:
                    obj_class = m.get_class(model, link["id2"])
                    assets.append(obj_class + ":" + link["id2"])
                elif obj_id == link["id2"] and link["type1"] == assoc:
                    obj_class = m.get_class(model, link["id1"])
                    assets.append(obj_class + ":" + link["id1"])
        elif len(complex_atk) > 1:
            assoc = complex_atk.pop(0)
            for link in obj_links:
                if obj_id == link["id1"] and link["type2"] == assoc:
                    next_obj_id = link["id2"]
                    _resolve(complex_atk, next_obj_id)
                elif obj_id == link["id2"] and link["type1"] == assoc:
                    next_obj_id = link["id1"]
                    _resolve(complex_atk, next_obj_id)

    _resolve(complex_atk, obj_id)
    return assets


def build_links_from_transitive_attack_step(model, obj_id: str, attack_name: str) -> list:
    """
    Returns a list of strings representing node IDs in an attack graph.
    The format is: `assetId:atkname`

    Arguments:
    model           - a SecuriCAD Model object
                      this can be retrieved for example by:
                      model = model_info.get_model()
    obj_id          - the ID of the asset object
    attack_name     - a string representing an external attack name


    Return:
    A list of tuples in the format (ObjectID:AttackName, class).
    """

    complex_atk = attack_name.split(".")
    connections = []
    visited_objects = [obj_id]

    def _resolve(complex_atk, obj_id):
        obj_links = m.get_links_for_object(model, obj_id)
        if len(complex_atk) == 2:
            assoc = complex_atk[0]
            if "*" in assoc:
                assocname = assoc[:-1]
                for link in obj_links:
                    if obj_id == link["id1"] and link["type2"] == assocname:
                        next_obj_id = link["id2"]
                        obj_class = m.get_class(model, link["id2"])
                        connections.append(obj_class + ":" + link["id2"] + ":" + complex_atk[1])
                        _resolve(complex_atk, next_obj_id)
                    elif obj_id == link["id2"] and link["type1"] == assocname:
                        next_obj_id = link["id1"]
                        obj_class = m.get_class(model, link["id1"])
                        connections.append(obj_class + ":" + link["id1"] + ":" + complex_atk[1])
                        _resolve(complex_atk, next_obj_id)

        elif len(complex_atk) > 2:
            assoc = complex_atk.pop(0)
            for link in obj_links:
                # here we could also use out-links
                if obj_id == link["id1"] and link["type2"] == assoc:
                    next_obj_id = link["id2"]
                    _resolve(complex_atk, next_obj_id)
                elif obj_id == link["id2"] and link["type1"] == assoc:
                    next_obj_id = link["id1"]
                    _resolve(complex_atk, next_obj_id)
    _resolve(complex_atk, obj_id)
    return connections


def get_node_by_id(graph: List[AtkGraphNode], node_id: str) -> dict:
    for node in graph:
        if node.id == node_id:
            return node

def remove_nodes_by_id(graph: List[AtkGraphNode], node_ids: List[str]) -> dict:
    newgraph = []
    for node in graph:
        if node.id not in node_ids:
            newgraph.append(node.id)
    return newgraph


def get_node_parents(graph: List[AtkGraphNode], node_id: str) -> List[str]:
    parents = []
    for element in graph:
        for link in element.links:
            if node_id == link:
                parents.append(element.id)
    return parents





def where(node_id=None, node_type=None, cls=None, objid=None, is_reachable=None, atkname=None, is_traversable=None):
    def _(node):
        query = []
        query.append(node_id == node.id)                    if node_id        is not None else query.append(True)
        query.append(node_type == node.type)                if node_type      is not None else query.append(True)
        query.append(cls == node.objclass)                  if cls            is not None else query.append(True)
        query.append(objid == node.objid)                   if objid          is not None else query.append(True)
        query.append(atkname == node.atkname)               if atkname        is not None else query.append(True)
        query.append(is_reachable == node.is_reachable)     if is_reachable   is not None else query.append(True)
        query.append(is_traversable == node.is_traversable) if is_traversable is not None else query.append(True)
        return all(query)
    return _

def select(where_stmt_func, graph):
    return [node for node in graph if where_stmt_func(node)]




def attach_attacker(model: dict, graph: List[AtkGraphNode]) -> List[AtkGraphNode]:
    atk_ids = m.get_attacker_ids(model)
    for atk_id in atk_ids:
        atk_assocs = m.get_links_for_object(model,atk_id)
        links = []
        for assoc in atk_assocs:
            if assoc['id1'] == atk_id:
                atkname = assoc['type1'].split(".")[0]
                target_id = assoc['id2']
                target_class = m.get_class(model, assoc["id2"])
            elif assoc['id2'] == atk_id:
                atkname = assoc['type2'].split(".")[0]
                target_id = assoc['id1']
                target_class = m.get_class(model, assoc["id1"])
            links.append(target_class + ":" + target_id + ":" + atkname)

        new_node = AtkGraphNode(
                id="Attacker:" + atk_id + ":firstSteps",
                type="or",
                objclass="Attacker",
                objid=atk_id,
                atkname="firstSteps",
                ttc=None,
                links=links,
                graph_type = "attackgraph",
                is_reachable = True,
                required_steps = None,
                defense_status = None
        )
        graph.append(new_node)

    return graph


def build_assets_set(model, obj_id, rlhs):
    def get_asset_name(namexp):
        expr = ""

        def _get_name(namexp):
            if namexp["type"] == "field" or namexp["type"] == "variable" :
                return expr + namexp["name"] + "."
            # If it is a complex nested name
            elif namexp["type"] == "collect":
                return expr + \
                    _get_name(namexp["lhs"]) + _get_name(namexp["rhs"])
        return _get_name(namexp)

    def _buildexpr(rlhs):
        # If it is a union operation
        if rlhs["type"] in ["field", "collect", "variable"]:
            asset_name = get_asset_name(rlhs)[:-1]
            return set(build_links_from_variable_attack_step(model,obj_id,asset_name))
        elif rlhs["type"] == "union":
            return _buildexpr(rlhs['lhs']) | _buildexpr(rlhs['rhs'])
        elif rlhs["type"] == "intersection":
            return _buildexpr(rlhs['lhs']) & _buildexpr(rlhs['rhs'])
        elif rlhs["type"] == "difference":
            return _buildexpr(rlhs['lhs']) - _buildexpr(rlhs['rhs'])

    return _buildexpr(rlhs)


def generate_graph(lang: dict, model: dict) -> List[AtkGraphNode]:
    """
    Generate attack graph starting from a model instance
    and a MAL language specification

    Arguments:
    model           - a SecuriCAD Model dictionary
                      this can be retrieved for example by:
                        model = model_info.get_model().model
                      or by using the mgg.securicad module function
                        `get_model`
    lang_spec       - a dictionary representing the whole grammar of the
                      a mal language as provided by `read_language_spec(...)`

    Return:
    A list of dictionaries, where each dictionary represents a node.
    Each node has its links in the "links" key
    """
    graph = []
    objects = m.get_objects(model)

    for obj_id in objects:
        obj_class = m.get_class(model, obj_id)
        print(obj_class)
        atks = l.get_attacks_for_class(lang, obj_class)
        print(json.dumps(atks, indent = 2))
        for atk_name, attribs in atks.items():
            atksteps = l.get_child_steps(lang, obj_class, atk_name)
            links = []
            for atkstep in atksteps:
                if "\\/" in atkstep or "/\\" in atkstep or "-" in atkstep:
                    set_vars = atks[atk_name]['reaches']['stepExpressions']
                    asset_set = build_assets_set(model, obj_id, let_var)
                elif "[" in atkstep:
                    print("SQUARED ATKSTEP")
                    print(atkstep)
                    base, atkname = atkstep.split(".")
                    relname = base.split("[")[0]
                    subclass = base.split("[")[1][:-1]
                    links = build_links_from_square_attack_step(model,obj_id,relname)
                    for link in links:
                        if link.split(":")[0] == subclass:
                            links.append(link + ":" + atkname)

                elif "()" in atkstep:
                    atkname = atkstep.split(".")[-1]
                    varname = atkstep.split("()")[0]
                    let_var = l.get_variables_for_class(lang,obj_class)[varname]['stepExpression']
                    asset_set = build_assets_set(model, obj_id, let_var)
                    for asset in asset_set:
                        links.append(asset + ":" + atkname)

                elif "*" in atkstep:
                    atkname = atkstep
                    links += build_links_from_transitive_attack_step(model, obj_id, atkstep)

                elif l.is_internal_attack_step(atkstep):
                    atkname = atkstep
                    links.append(
                        build_link_from_internal_attack_step(
                            obj_id, atkstep, obj_class))
                else:
                    atkname = atkstep.split(".")[-1]
                    links += build_links_from_external_attack_step(
                        model, obj_id, atkstep)
            if attribs["type"] == "or":
                is_reachable = True
            else:
                is_reachable = False

            if attribs["type"] in ["or","and"]:
                req_steps = l.get_parent_steps(lang, obj_class, atkname)
                req_steps = [str(r[0] + ":" + r[1]) for r in req_steps]
            else:
                req_steps = None

            defense_status = None
            if attribs["type"] == "defense":
                print("THIS IS A DEFENSE STEP")
                print(atk_name)
                if 'active_defense' in objects[obj_id]:
                    print("THERE IS ACTIVE DEFENSE IN DICTIONARY")
                    print("PRINTING WHOLE ACTIVE DEFENSE SECTION:")
                    print(objects[obj_id]['active_defense'])
                    if atk_name in objects[obj_id]['active_defense']:
                        defense_status = objects[obj_id]['active_defense'][atk_name]
                else:
                    defense_status = "unset"

            new_node = AtkGraphNode(
                    id=obj_class + ":" + obj_id + ":" + atk_name,
                    type=attribs["type"],
                    objclass=obj_class,
                    objid=obj_id,
                    atkname=atk_name,
                    ttc=attribs["ttc"],
                    links=links,
                    graph_type = "attackgraph",
                    is_reachable = is_reachable,
                    required_steps = req_steps,
                    defense_status = defense_status
            )
            graph.append(new_node)
    return graph

